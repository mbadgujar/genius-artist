package org.sample.genius.util

import com.sample.genius.util.HttpUtil.Client
import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.entity.{ContentType, StringEntity}
import org.apache.http.message.{BasicHttpResponse, BasicStatusLine}
import org.apache.http.{HttpResponse, HttpVersion}

import scala.io.Source

class TestClient extends Client{

  private def fileUtil(name: String) : String = {
    val cl = this.getClass.getClassLoader
    Source.fromInputStream(cl.getResourceAsStream("./" + name)).getLines().mkString
  }

  private def createJsonResponse(jsonString: String): HttpResponse = {
    val response = new BasicHttpResponse(
      new BasicStatusLine(HttpVersion.HTTP_1_1, 200, "success"))
    response.setEntity(new StringEntity(jsonString, ContentType.APPLICATION_JSON))
    response
  }


  override def getResponseFunc[T <: HttpRequestBase]: T => HttpResponse =
    (request: T) => request.getURI.toString match {
      case url if url.contains("/search") => createJsonResponse(fileUtil("search.json"))
      case url if url.contains("/songs") => createJsonResponse(fileUtil("songs.json"))
    }

  override def close(): Unit = {}
}
