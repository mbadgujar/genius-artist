package org.sample.genius

import com.sample.genius.FetchArtistSongs
import com.sample.genius.FetchArtistSongs.InputConfig
import org.sample.genius.util.TestClient
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class MappingTest extends AnyFlatSpec with should.Matchers{

  "Genius json responses " should "be correctly mapped" in {
    System.setProperty("GENIUS_API_TOKEN", "dummy")
    val result = FetchArtistSongs.searchAndFetch(new TestClient(), InputConfig(artistName = "artist1"))
    result.size should be(2)
  }

}
