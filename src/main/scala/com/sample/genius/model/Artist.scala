package com.sample.genius.model

import com.fasterxml.jackson.annotation.JsonProperty


case class ArtistSearch(hits: List[Artist]) {
  def containsArtist(artist: String): Option[Artist] = {
    hits.find {
      artistInResult => artistInResult.name.toLowerCase.trim == artist.toLowerCase.trim
    }
  }
}

case class Artist(var id: Long, var name: String) {
  @JsonProperty("result")
  private def unnestArtist(result: Map[String, Any]): Unit = {
    val artistInfo = result("primary_artist").asInstanceOf[Map[String, Any]]
    this.id = artistInfo("id").asInstanceOf[Int]
    this.name = artistInfo("name").asInstanceOf[String]
  }
}
