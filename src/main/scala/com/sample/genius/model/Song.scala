package com.sample.genius.model

import com.fasterxml.jackson.annotation.JsonProperty

case class Song(title: String, url: String) {
  override def toString: String = {
    s"Song: $title, Lyrics: $url"
  }
}
case class SongList(songs: List[Song], @JsonProperty(value = "next_page") nextPage: Option[Int])