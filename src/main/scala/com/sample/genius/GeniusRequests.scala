package com.sample.genius

import org.apache.http.client.methods.{HttpGet, HttpRequestBase}
import org.apache.http.client.utils.URIBuilder

object GeniusRequests {

  val GeniusAPI = "api.genius.com"

  val Token: String = Option(System.getProperty("GENIUS_API_TOKEN"))
    .fold(throw new Exception("Please set GENIUS_API_TOKEN property.")){
      token => token
    }

  sealed trait Request[T <: HttpRequestBase] {
    def getRequest: T
  }

  case class SearchRequest(searchQuery: String) extends Request[HttpGet] {
    override def getRequest: HttpGet = {
      val uri = new URIBuilder()
        .setScheme("https")
        .setHost(GeniusAPI)
        .setPath("/search")
        .setParameter("q", searchQuery)
        .build()
      val call = new HttpGet(uri)
      call.setHeader("Authorization", "Bearer " + Token)
      call
    }
  }

  case class SongsRequest(artistId: Long, page: Int = 1) extends Request[HttpGet] {
    override def getRequest: HttpGet = {
      val uri = new URIBuilder()
        .setScheme("https")
        .setHost(GeniusAPI)
        .setPath(s"/artists/$artistId/songs")
        .setParameter("sort", "popularity")
        .setParameter("per_page", "50")
        .setParameter("page", page.toString)
        .build()
      val call = new HttpGet(uri)
      call.setHeader("Authorization", "Bearer " + Token)
      call
    }
  }

}
