package com.sample.genius.util

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.{DefaultScalaModule, ScalaObjectMapper}
import com.sample.genius.GeniusRequests
import com.sample.genius.util.HttpUtil.Client
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.impl.client.HttpClients

class HttpUtil(client: Client) {

  private val mapper = new ObjectMapper() with ScalaObjectMapper
  mapper.registerModule(DefaultScalaModule)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  def fetchAndConvert[B <: Any](request: GeniusRequests.Request[_ <: HttpRequestBase],
                                convertValueKey: String)
                               (implicit manifest: Manifest[B]): B = {
    val entity = client.getResponseFunc(request.getRequest).getEntity
    val map = mapper.readValue[Map[String, Any]](entity.getContent)
    mapper.convertValue[B](map(convertValueKey))
  }
}


object HttpUtil {

  trait Client {
    def getResponseFunc[T <: HttpRequestBase]: T => HttpResponse

    def close(): Unit
  }

  // Default client implementation using Apache HTTP default Client
  object DefaultClient extends Client {
    private val client = HttpClients.createDefault()

    override def getResponseFunc[T <: HttpRequestBase]: T => HttpResponse = (request: T) => client.execute(request)

    override def close(): Unit = client.close()
  }

}