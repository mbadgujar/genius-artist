package com.sample.genius

import com.sample.genius.GeniusRequests.{SearchRequest, SongsRequest}
import com.sample.genius.model.{ArtistSearch, Song, SongList}
import com.sample.genius.util.HttpUtil
import com.sample.genius.util.HttpUtil.Client
import scopt.OParser

object FetchArtistSongs {

  case class InputConfig(artistName: String = "")

  private def getSongs(songFetchFunction: Int => SongList, page: Int = 1): List[Song] = {
    val songList = songFetchFunction(page)
    var nextPage = songList.nextPage
    var mergedSongList = songList.songs
    while (nextPage.isDefined) {
      val nextSongList = songFetchFunction(nextPage.get)
      nextPage = nextSongList.nextPage
      mergedSongList = mergedSongList ++ nextSongList.songs
    }
    mergedSongList
  }

  def searchAndFetch(client: Client, config: InputConfig): List[Song] = {
    val httpUtil = new HttpUtil(client)
    val artist = config.artistName
    try {
      // Check if Artist is present on Genius
      val artistSearchResult = httpUtil
        .fetchAndConvert[ArtistSearch](SearchRequest(artist), "response")
        .containsArtist(artist)

      artistSearchResult match {
        case None =>
          throw new Exception(s"Artist $artist not found.")
        // Get all songs for the artist
        case Some(artist) =>
          val songFetchFunction = (page: Int) => httpUtil
            .fetchAndConvert[SongList](SongsRequest(artist.id, page), "response")
          getSongs(songFetchFunction)
      }
    } finally {
      client.close()
    }
  }

  def main(args: Array[String]): Unit = {
    val builder = OParser.builder[InputConfig]
    val parser = {
      import builder._
      OParser.sequence(
        programName("GeniusArtist: List all songs by an Artist from Genius Database"),
        opt[String]('a', "artist")
          .required()
          .validate(str => if (str.trim.nonEmpty) success else failure("Artist name is empty"))
          .action((x, s) => s.copy(artistName = x))
          .text("Name of the Artist")
      )
    }
    OParser.parse(parser, args, InputConfig()) match {
      case Some(config) =>
        val client = HttpUtil.DefaultClient
        searchAndFetch(client, config).foreach(println)
      case _ =>
        throw new Exception("Invalid arguments provided")
    }

  }

}
