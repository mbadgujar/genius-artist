# README #
A small application to get all the songs of an artist from Genius.

### How to Build and Run ###

* Install JDK 1.8 and maven
* Create a genuis API token by following the document: https://docs.genius.com/#/getting-started-h1. (Click on Generate access token to create client access token once you have created an API client)
* Clone the project and run _mvn clean install_ from the project root
* After building the project, run following command:
  java -cp target/genius-artist-1.0-jar-with-dependencies.jar -DGENIUS_API_TOKEN=<genius_api_token>  com.sample.genius.FetchArtistSongs --artist=<artist_name>
  
### Docker Run ###
You can also run the application via docker if it is more convenient. (You still need to create the API token)

* Change directory to docker folder and run _docker build -t genius-artist ._ to build docker image
* once built run the application using following command: docker run -e GENIUS_API_TOKEN=<genius_api_token> -e ARTIST=<artist_name> genius-artist

